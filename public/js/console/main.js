$(document).ready(function(){
    $("#command_form").keypress(function(e){
        if(e.which == 13) {
            let value = $("[name='command']").val();
            webconsole.writeline(value);
            switch(value){
                case "clear":
                    webconsole.clear();
                    break;
                default:
                    webconsole.sendcommand(value);
            }
            $("[name='command']").val("");
            return false;
        }
    });
    $("#serveraddress").text(window.location.hostname);
});