var webconsole = {
    writeline: function(text){
        $("#console_content").text($("#console_content").text()+text+'\n');
        this.scrolldown();
    },
    clear: function(){
        $("#console_content").text("");
    },
    scrolldown: function(){
        $("#console_content").scrollTop($("#console_content")[0].scrollHeight);
    },
    sendcommand: function(command){
        let currentObj = this;
        $.ajax({
            url: '/console/command',
            data: {command: command},
            method: 'post',
            success: function(output){
                console.log(output);
                currentObj.writeline(output);
            }
        });
    }
};