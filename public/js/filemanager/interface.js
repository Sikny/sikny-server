function showFiles(){
    var currentDir =  $("[name='currentPath']").val();
    $.ajax({
        method: "post",
        url: '/storage',
        data: { path: currentDir },
        datatype: 'json',
        success: function(result){
            $("#files_zone").html("");
            $("[name='currentPath']").val(result.currentDir==''?'/':result.currentDir);
            $.each(result.files, function(index, file){
                $("#files_zone").append("<div class='file-item'>"
                + "<div class='file-item-icon'>"
                + "<img src='" + file.icon
                + "' width='16' height='16' /></div>"
                + "<p>" + file.name + "</p>"
                + buildContextMenu(file) + "</div>");
                $(".file-item").eq(index).find(".file-context-menu .file-download div").click(function(){
                    senddownloadrequest(file);
                })
            });
            currentFolder = result;
            bindFileEvents();
        }
    });
}

function buildContextMenu(file){
    let contextmenu = "<div style='display: none;' class='file-context-menu'>"
        + "<ul>"
        + "<li><div class='file-download'><div class='context-menu-icon icon-download'></div><span>Download</span></div></li>"
        + "<li><div onclick='copyFile(\"" + file.path + "\")'><div class='context-menu-icon'></div><span>Copy</span></div></li>"
        + "<li><div onclick='removeFile(\"" + file.path + "\")'><div class='context-menu-icon icon-trash'></div><span>Delete</span></div></li>"
        + "<li><div onclick='renameFile(\"" + file.path + "\", \"" + file.name + "\")'><div class='context-menu-icon'></div><span>Rename</span></div></li>"
        + "</ul>"
        + "</div>";
    return contextmenu;
}

function bindFileEvents(){
    $(document).click(function(event){
        $(".file-context-menu").hide();
    });
    $(".file-item").dblclick(function(){
        if($("[name='currentPath']").val() == '/')
            $("[name='currentPath']").val('');
        $("[name='currentPath']").val($("[name='currentPath']").val()
            + '/' + $(this).find("p").text());
        showFiles();
    });
    $(".file-item").click(function(event){
        selectFile($(this), event);
    });
    $(".file-item").contextmenu(function(event){
        selectFile($(this), event);
        $(this).find(".file-context-menu").show();
        $(this).find(".file-context-menu").css("left", event.pageX);
        $(this).find(".file-context-menu").css("top", event.pageY);
        return false;
    });
    // draggable
    
}

function selectFile(item, event){
    if(!fileSelected || fileSelected.text() != $(item).text()){
        if(fileSelected)
            fileSelected.removeClass('selected');
        $(".file-item").removeClass('was-selected');
        fileSelected = $(item);
        fileSelected.addClass('selected');
    } else if(event.which == 1) {
        fileSelected.removeClass('selected');
        fileSelected.addClass('was-selected');
        fileSelected = undefined;
    }
    displayInfo();
}

function displayInfo(){
    if(fileSelected){
        var index = $(".file-item").index(fileSelected);
        $("#info_pane .fileinfo_name").text(currentFolder.files[index].name);
        $("#info_pane .fileinfo_type").text(currentFolder.files[index].type)
        // date
        $("#info_pane .fileinfo_dateupload .fileinfo_label").text("Date uploaded: ");
        $("#info_pane .fileinfo_dateupload .fileinfo_data").text(currentFolder.files[index].stat.birthtime.replace('T', ' ').replace('Z', '').split('.')[0]);
        // size
        $("#info_pane .fileinfo_size .fileinfo_label").text("Size:");
        if(currentFolder.files[index].is_dir)
            $("#info_pane .fileinfo_size .fileinfo_data").text('Calculating...');
        else 
            $("#info_pane .fileinfo_size .fileinfo_data").text(currentFolder.files[index].stat.size+" bytes");
        if(currentFolder.files[index].is_dir)
            calculateFolderSize(currentFolder.files[index].path);
        if(currentFolder.files[index].name != '..'){
            $("#info_pane .fileinfo_download button").show();
            $("#info_pane .fileinfo_download button").text('Download');
        } else {
            $("#info_pane .fileinfo_download button").hide();
        }
    }
}

function calculateFolderSize(folderpath){
    $.ajax({
        url: '/folder/getsize',
        data: { path: folderpath },
        method: "post",
        datatype: 'json',
        success: function(result){
            $("#info_pane .fileinfo_size .fileinfo_data").text(result.foldersize);
        }
    });
}

function senddownloadrequest(file){
    console.log(file);
    if(!file.is_dir)
        window.open(file.path.split('/public')[1], '_blank');
    else {
        $.ajax({
            url: '/folder/zip',
            data: {path: file.path},
            method: 'post',
            datatype: 'json',
            success: function(result){
                window.open(result.archivepath.split('/public')[1], '_blank');
                removeFile(result.archivepath);
            }
        });
    }
}

function removeFile(filepath){
    $.ajax({
        url: '/file/delete',
        data: {path: filepath},
        method: 'post',
        datatype: 'json',
        success: function(result){
            showFiles();
        }
    });
}

function triggerupload(){
    $("#file-uploader").click();
}

function uploadFile(){
    let files = $("#file-uploader")[0].files;
    $.each(files, function(index, file){
        let formdata = new FormData();
        formdata.append('file', file);
        formdata.append('currentdir', 'public/files'+(currentFolder.currentDir==''?'/':currentFolder.currentDir));
        $.ajax({
            url: '/file/upload',
            data: formdata,
            cache: false,
            method: 'post',
            contentType: false,
            processData: false,
            success: function(result){
                showFiles();
            }
        });
    });
}

function createFolder(){
    let folderName = prompt("New folder name ?", "New folder");
    $.ajax({
        url: '/folder/create',
        data: {
            foldername: folderName,
            currentdir: (currentFolder.currentDir==''?'/':currentFolder.currentDir)
        },
        method: 'post',
        datatype: 'json',
        success: function(result){
            showFiles();
        }
    });
}

function copyFile(filepath){
    copiedFile = filepath;
}

function pasteFile(){
    $.ajax({
        url: '/file/copy',
        data: {
            filepath: copiedFile,
            currentdir: (currentFolder.currentDir==''?'/':currentFolder.currentDir)
        },
        method: 'post',
        success: function(result){
            console.log(result);
            showFiles();
        }
    });
}

function renameFile(filepath, filename){
    let newname = prompt("Rename", filename);
    if(newname != ''){
        $.ajax({
            url: '/file/rename',
            data: {
                filepath: filepath,
                currentdir: (currentFolder.currentDir==''?'/':currentFolder.currentDir),
                newname: newname
            },
            method: 'post',
            success: function(result){
                console.log(result);
                showFiles();
            }
        });
    }
}