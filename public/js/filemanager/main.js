var fileSelected;
var currentFolder = {};
var copiedFile;

$(document).ready(function() {
    $("#uploadfilebutton").click(function(){
        $("#hiddenfileinput").val(null);
        $("#hiddenfileinput").trigger("click");
    });
    $("#hiddenfileinput").change(function(){
        uploadFiles();
    });
    $("#changePathForm").submit(function(evt) {
        evt.preventDefault();
        showFiles();
    });
    $("#info_pane .fileinfo_download button").on("click", function(){
        let index = $(".file-item").index(fileSelected);
        senddownloadrequest(currentFolder.files[index]);
    });
    $("#main_zone").contextmenu(function(){
        $("#context-menu-files_zone").show();
        $(this).find("#context-menu-files_zone").css("left", event.pageX);
        $(this).find("#context-menu-files_zone").css("top", event.pageY);
        return false;
    });
    showFiles();
    displayInfo();
});