const port = 8080;
const express = require('express');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const fs = require('fs'); // filesystem
const databaseapi = require('./js_modules/database/databaseapi');
const fileApi = require('./js_modules/filemanager/fileapi');
const themeApi = require('./js_modules/themeapi');
const logApi = require('./js_modules/logs/logapi');
const archiver = require('archiver');
const formidable = require('formidable');   // file upload
const child_process = require('child_process');

const app = express();
app.use(express.static(__dirname + '/public'));

var urlEncodedParser = bodyParser.urlencoded({ extended: false });

app.use(cookieParser());
app.use(session({
    secret: 'log key',
    resave: false,
    saveUninitialized: true
}));

app.set('view engine', 'ejs');

var server = app.listen(port, function(){
    console.log("Server listening on port " + port + ".");
});

app.get('/', function(req, res) {
    let theme = themeApi.getthemepath();
    if(req.session.login){
        res.render('pages/index', {
            themepath: theme, 
            username: req.session.login, 
            serveraddress: server.address().address
        });
    } else {
        res.render('pages/login', {message: '', themepath: theme});
    }
});

app.get('/register', function(req, res) {
    let theme = themeApi.getthemepath();
    res.render('pages/register', {message: '', themepath: theme});
});

app.post('/login', urlEncodedParser, function(req, res){
    let theme = themeApi.getthemepath();
    databaseapi.getusers(req.body.login, req.body.password, function(results){
        let logValue = {};
        if(results.length > 0){
            logValue = logApi.buildlog(req.body.login, "Successfully logged in");
            req.session.login = results[0].login;
            console.log("User " + results[0].login + " connected");
            res.redirect('/');
        } else {
            logValue = logApi.buildlog(req.body.login, "Unsuccessfully tried to log in");
            res.render('pages/login', {message: "No such login/password combination", themepath: theme});
        }
        databaseapi.insertlog(logValue);
    });
});
app.post('/register', urlEncodedParser, function(req, res){
    let theme = themeApi.getthemepath();
    databaseapi.existinguser(req.body.login, function(results){
        if(results.length > 0){
            res.render('pages/register', {message: "Login already used", themepath: theme});
        } else if(req.body.password != req.body.conf_password){
            res.render('pages/register', {message: "Password don't match", themepath: theme});
        } else {
            databaseapi.insertuser(req.body.login, req.body.password, function(){
                res.render('pages/login', {message: "Registered, please log in", themepath: theme});
            });
        }
    });
});

app.get('/logout', function(req, res){
    if(req.session.login){
        let logValue = logApi.buildlog(req.session.login, "Logged out");
        databaseapi.insertlog(logValue);
    }
    req.session.destroy(function(error){
        if(error) throw error;
        res.redirect('/');
    });
});

/** STORAGE **/
app.get('/storage', function(req, res){
    if(req.session.login){
        let theme = themeApi.getthemepath();
        res.render('pages/storage', {curdirectory: '/', themepath: theme});
    } else res.redirect('/');
});
app.post('/storage', urlEncodedParser, function(req, res){
    let userPath = req.body.path;
    if(userPath != '' && userPath[0] != '/')
        userPath = '/' + userPath;
    let directory = ('public/files'+userPath).trim();
    let dirResolved = path.resolve(directory).replace(/\\/g, '/');
    if(!fs.existsSync(directory) || !fs.statSync(directory).isDirectory()
            || !dirResolved.includes('public/files'))
        dirResolved = path.resolve('public/files').replace(/\\/g, '/');
    let fileNames = fs.readdirSync(dirResolved);
    if(dirResolved != path.resolve('public/files').replace(/\\/g, '/')){
        fileNames.unshift('..');
    }
    let files = [];
    for(let index = 0; index < fileNames.length; index++){
        if(fileNames[index] != '..') {
            fileResolved = dirResolved+'/'+fileNames[index];
            files[index] = fileApi.getfilestat(fileResolved);
        } else
            files[index] = fileApi.getpreviouspath(dirResolved);
    }
    files.sort(fileApi.comparesorttype);
    let sendingPath = dirResolved.split('public/files')[1];
    if(sendingPath == undefined) sendingPath = '/';
    res.send({files: files, currentDir: sendingPath});
});
app.post('/folder/getsize', urlEncodedParser, function(req, res){
    let folderPath = req.body.path;
    let foldersize = fileApi.getfoldersize(folderPath);
    res.send({foldersize: foldersize});
});
app.post('/folder/zip', urlEncodedParser, function(req, res){
    let folderpath = req.body.path;
    let output = fs.createWriteStream(folderpath + '.zip');
    let archive = archiver('zip', {zlib: {level: 9}});
    archive.pipe(output);
    archive.directory(folderpath, path.basename(folderpath));
    archive.finalize();
    res.send({archivepath: folderpath + '.zip'});
});
app.post('/folder/create', urlEncodedParser, function(req, res){
    let foldername = req.body.foldername;
    let currentdir = req.body.currentdir;
    if(currentdir[currentdir.length-1] != '/') currentdir += '/';
    fs.mkdir('public/files'+currentdir+foldername, function(err){
        if(err) throw err;
        res.send({status: 'success'});
    });
});
app.post('/file/delete', urlEncodedParser, function(req, res){
    let filepath = req.body.path;
    let result = fileApi.deleterec(filepath);
    res.send({status: result});
});
app.post('/file/upload', urlEncodedParser, function(req, res){
    let form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files){
        if(files.file.size > 5000000) {
            res.send({status: 'file too big'});
            return;
        }
        if(err) throw err;
        let oldpath = files.file.path;
        let newpath = fields.currentdir + '/' + files.file.name;
        if(newpath.search('public/files') != -1){
            fs.rename(oldpath, newpath, function(err){
                if(err) {res.send(err); return;}
                res.send({status: 'success'});
                return;
            });
        } else {
            unlink(newpath);
            res.send({status: 'unauthorized', requestedpath: newpath});
            return;
        }
    });
});
app.post('/file/copy', urlEncodedParser, function(req, res){
    let filepath = req.body.filepath;
    let currentdir = 'public/files'+req.body.currentdir;
    let status = fileApi.copyrec(filepath, currentdir+'/'+path.basename(filepath));
    res.send({status: status});
});
app.post('/file/rename', urlEncodedParser, function(req, res){
    let filepath = req.body.filepath;
    let currentdir = 'public/files'+req.body.currentdir;
    let newname = req.body.newname;
    fs.rename(filepath, currentdir+'/'+newname, function(err){
        let status = 'success';
        if(err) status = 'error: '+err;
        res.send({status: status});
    });
});

/** LOGS **/
app.get('/logs', urlEncodedParser, function(req, res){
    if(req.session.login){
        let theme = themeApi.getthemepath();
        databaseapi.getlogs(function(results){
            if(results.length > 0){
                let logs = [];
                results.forEach(log => {
                    logs.push("<tr>"
                        + "<td>"+log.log_user+"</td>"
                        + "<td>"+logApi.parsedate(log.date)+"</td>"
                        + "<td>"+log.detail+"</td>"
                    + "</tr>");
                });
                res.render('pages/logs', {themepath: theme, logs: logs});
            } else {
                res.render('pages/logs', {themepath: theme, logs: []});
            }
        });
    } else res.redirect('/');
});

/** CONSOLE **/
app.post('/console/command', urlEncodedParser, function(req, res){
    let command = req.body.command;
    child_process.exec(command, {shell: true}, function(error, stdout, stderr){
        if(error) console.error(error);
        if(stderr) {res.send(stderr); }
        else if(stdout) {res.send(stdout); }
    });
});

// redirection
app.use(function(req, res, next) {
    res.redirect('/');
});