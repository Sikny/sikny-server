const path = require('path');
const fs = require('fs');
const extensions = require('./extensions');
const themeApi = require('../themeapi');

module.exports = {
    /**
     * @param {String} filepath resolved file path
     * @returns {object} file object
     */
    getfilestat: function(filepath){
        let filestat = fs.statSync(filepath);
        return {
            name: path.basename(filepath),
            icon: this.getfileicon(filestat, themeApi.getthemeconfig()),
            is_dir: filestat.isDirectory(),
            path: filepath,
            stat: filestat,
            type: extensions.getextensiondesc(path.extname(filepath).substr(1), filestat.isDirectory())
        };
    },
    /**
     * @param {fs.Stats} stat fs.Stats object
     * @param {object} config json data
     * @returns {String} icon path 
     */
    getfileicon: function(stat, config){
        let themePath = 'themes/'+themeApi.theme+"/icon/";
        if(stat.isDirectory()) return themePath+config.ico.folder;
        else return themePath+config.ico.unknown;
    },
    /**
     * @param {String} dirpath current folder path
     * @returns {object} previous folder file object
     */
    getpreviouspath: function(dirpath){
        let filestat = fs.statSync(path.resolve(dirpath+'/..'));
        return {
            name: '..',
            icon: 'themes/'+themeApi.theme+'/icon/'+themeApi.getthemeconfig().ico.folder,
            is_dir: true,
            stat: filestat,
            path: dirpath,
            type: extensions.getextensiondesc('', true)
        };
    },
    /**
     * @param {String} folderpath folder resolved path
     * @returns {number} total size of folder
     */
    getfoldersize: function(folderpath){
        let totalsize = 0;
        let moduleref = this;
        let files = fs.readdirSync(folderpath);
        for(let index = 0; index < files.length; index++){
            let curfilepath = folderpath+'/'+files[index];
            filestat = fs.statSync(curfilepath);
            if(!filestat.isDirectory()){
                totalsize += filestat.size;
            } else {
                totalsize += moduleref.getfoldersize(curfilepath);
            }
        }
        return totalsize;
    },
    /**
     * @param {object} file1 file object
     * @param {object} file2 file object
     * @returns {number} comparison result
     */
    comparesorttype: function(file1, file2){
        if(file1.is_dir && !file2.is_dir)
            return -1;
        else if(!file1.is_dir && file2.is_dir)
            return 1;
        else{
            return file1.name.localeCompare(file2.name);
        }
    },
    /**
     * @param {string} src the source folder
     * @param {string} dest destination name
     * @returns {string} success or error
     */
    copyrec: function(src, dest){
        fileapi = this;
        if(!fs.statSync(src).isDirectory()){
            fs.copyFile(src, dest, function(err){
                if(err) throw err;
                return 'success';
            });
        } else {
            fs.mkdirSync(dest, {recursive: true});
            let files = fs.readdirSync(src);
            files.forEach(function(value, index, array){
                fileapi.copyrec(src+'/'+value, dest+'/'+value);
            });
            return 'success';
        }
    },
    /**
     * @param {string} path file/folder to delete
     * @returns {string} success or error
     */
    deleterec: function(path){
        fileapi = this;
        if(!fs.statSync(path).isDirectory()){
            fs.unlinkSync(path);
            return 'success';
        } else {
            let files = fs.readdirSync(path);
            files.forEach(function(value, index, array){
                fileapi.deleterec(path+'/'+value);
            });
            fs.rmdirSync(path);
            return 'success';
        }
    }
};