module.exports = {
    /**
     * @param {String} ext file extension
     * @param {boolean} is_dir if given file is a directory or not
     * @returns {String} file type description
     */
    getextensiondesc: function(ext, is_dir = false){
        if(ext == '') return is_dir?'Folder':'File';
        else if(!this.extension.hasOwnProperty(ext)){
            return (ext.toUpperCase()+' file');
        } else return this.extension[ext];
    },
    extension: {
        'txt': 'Text Document',
        'pro': 'Qt Project file',
        'user': 'Per-User Project Options File',
        'js': 'JavaScript File',
        'css': 'Cascading Style Sheet Document',
        'exe': 'Application',
        'dll': 'Application extension',
        'docx': 'Microsoft Word Document',
        'h': 'Header file',
        'cpp': 'C++ source file',
        'json': 'JSON source file',
        'ini': 'Configuration settings',
        '.config': 'XML Configutation File',
        
    }
}