const mysql = require('mysql');

module.exports = {
    connection: mysql.createConnection({
        host    : 'localhost',
        user    : 'sikny',
        password: 'wnc4qsf5vmbr',
        database: 'sikny_storage'
    }),
    /**
     * @param {string} login 
     * @param {string} password 
     * @param {function(any)} callback arg0: values
     */
    getusers: function(login = "", password = "", callback){
        let sql = "SELECT * FROM `user` WHERE 1";
        let inserts = [];
        if(login != "") {
            sql += " AND `login`=?";
            inserts.push(login);
        }
        if(password != ""){
            sql += " AND `password`=MD5(?)";
            inserts.push(password);
        }
        sql = mysql.format(sql, inserts);
        this.connection.query(sql, function(err, results, fields){
            if(err) throw err;
            callback(results);
        });
    },
    existinguser: function(login, callback){
        let sql = "SELECT * FROM `user` WHERE `login`='"+login+"'";
        this.connection.query(sql, function(err, results, fields){
            if(err) throw err;
            callback(results);
        })
    },
    /**
     * @param {function(any)} callback arg0: values
     */
    getlogs: function(callback){
        this.connection.query('SELECT * FROM `logs` ORDER BY id DESC',
            function(error, results, fields){
                if(error) throw error;
                callback(results);
            }
        );
    },
    /**
     * @param {Object} log log object {log_user, date, detail}
     */
    insertlog: function(log){
        this.connection.query(
            "INSERT INTO `logs` (`log_user`, `date`, `detail`) VALUES (?, ?, ?)",
            [log.log_user, log.date, log.detail],
            function(err){ 
                if(err) throw err; 
            }
        );
    },
    /**
     * @param {string} login 
     * @param {string} password 
     */
    insertuser: function(login, password, callback){
        this.connection.query(
            "INSERT INTO `user` (`login`, `password`) VALUES (?, MD5(?))",
            [login, password],
            function(err){
                if(err) throw err;
                callback();
            }
        )
    }
};