module.exports = {
    /**
     * @param {string} log_user
     * @param {string} detail
     * @return {object} log object to insert in db
     */
    buildlog: function(log_user, detail){
        return {
            log_user: log_user,
            date: new Date(),
            detail: detail
        };
    },
    /**
     * @param {Date} date 
     * @return {string} [dd/mm/yy|HH:ii:ss]
     */
    parsedate: function(date){
        return "["
        +("0"+date.getDay()).slice(-2)+"/"+("0"+date.getMonth()).slice(-2)
        +"/"+("0"+date.getFullYear()).slice(-2)+" | "+("0"+date.getHours()).slice(-2)
        +":"+("0"+date.getMinutes()).slice(-2)+":"+("0"+date.getSeconds()).slice(-2)
        +"]";
    },
};