const fs = require('fs');
const path = require('path');

module.exports = {
    /**
     * @returns {object} json config data
     */
    getthemeconfig: function(){
        let themePath = path.resolve('public/themes/'+this.theme+"/config.json");
        return JSON.parse(fs.readFileSync(themePath));
    },
    /**
     * @returns {String} theme path
     */
    getthemepath: function(){
        return '/themes/'+this.theme;
    },
    theme: "windows_10"
};